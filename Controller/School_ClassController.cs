﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectTemp.Model;

namespace ProjectTemp.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class School_ClassController : ControllerBase
    {
        private readonly DbContextClass _context;

        public School_ClassController(DbContextClass context)
        {
            _context = context;
        }

        // GET: api/School_Class
        [HttpGet]
        public async Task<ActionResult<IEnumerable<School_Class>>> GetSchool_Class()
        {
            return await _context.School_Class.ToListAsync();
        }

        // GET: api/School_Class/5
        [HttpGet("{id}")]
        public async Task<ActionResult<School_Class>> GetSchool_Class(int id)
        {
            var school_Class = await _context.School_Class.FindAsync(id);

            if (school_Class == null)
            {
                return NotFound();
            }

            return school_Class;
        }

        // PUT: api/School_Class/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSchool_Class(int id, School_Class school_Class)
        {
            if (id != school_Class.Id)
            {
                return BadRequest();
            }

            _context.Entry(school_Class).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!School_ClassExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/School_Class
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<School_Class>> PostSchool_Class(School_Class school_Class)
        {
            _context.School_Class.Add(school_Class);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSchool_Class", new { id = school_Class.Id }, school_Class);
        }

        // DELETE: api/School_Class/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<School_Class>> DeleteSchool_Class(int id)
        {
            var school_Class = await _context.School_Class.FindAsync(id);
            if (school_Class == null)
            {
                return NotFound();
            }

            _context.School_Class.Remove(school_Class);
            await _context.SaveChangesAsync();

            return school_Class;
        }

        private bool School_ClassExists(int id)
        {
            return _context.School_Class.Any(e => e.Id == id);
        }
    }
}
