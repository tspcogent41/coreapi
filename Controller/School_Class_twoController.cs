﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectTemp.Model;

namespace ProjectTemp.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class School_Class_twoController : ControllerBase
    {
        private readonly DbContextClass _context;

        public School_Class_twoController(DbContextClass context)
        {
            _context = context;
        }

        // GET: api/School_Class_two
        [HttpGet]
        public async Task<ActionResult<IEnumerable<School_Class_two>>> GetSchool_Class_two()
        {
            return await _context.School_Class_two.ToListAsync();
        }

        // GET: api/School_Class_two/5
        [HttpGet("{id}")]
        public async Task<ActionResult<School_Class_two>> GetSchool_Class_two(int id)
        {
            var school_Class_two = await _context.School_Class_two.FindAsync(id);

            if (school_Class_two == null)
            {
                return NotFound();
            }

            return school_Class_two;
        }

        // PUT: api/School_Class_two/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSchool_Class_two(int id, School_Class_two school_Class_two)
        {
            if (id != school_Class_two.Id)
            {
                return BadRequest();
            }

            _context.Entry(school_Class_two).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!School_Class_twoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/School_Class_two
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<School_Class_two>> PostSchool_Class_two(School_Class_two school_Class_two)
        {
            _context.School_Class_two.Add(school_Class_two);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSchool_Class_two", new { id = school_Class_two.Id }, school_Class_two);
        }

        // DELETE: api/School_Class_two/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<School_Class_two>> DeleteSchool_Class_two(int id)
        {
            var school_Class_two = await _context.School_Class_two.FindAsync(id);
            if (school_Class_two == null)
            {
                return NotFound();
            }

            _context.School_Class_two.Remove(school_Class_two);
            await _context.SaveChangesAsync();

            return school_Class_two;
        }

        private bool School_Class_twoExists(int id)
        {
            return _context.School_Class_two.Any(e => e.Id == id);
        }
    }
}
