﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectTemp.Model;

namespace ProjectTemp.Model
{
    public class DbContextClass : DbContext
    {
        public DbContextClass(DbContextOptions<DbContextClass> options)
    : base(options)
        { }
        public DbSet<ProjectTemp.Model.School_Class> School_Class { get; set; }
        public DbSet<ProjectTemp.Model.School_Class_two> School_Class_two { get; set; }
        

    }
}
